# Style BibLaTeX du CRH

## Mots clefs

* Monographies : `keywords = {monographie}` ;

* Direction d'ouvrage : `keywords = {dirouvrage}` ;

* Direction de revue : `keywords = {dirrevue}` ;

* Éditions et éditions critiques : `keywords = {editions}` ;

* Préfaces et postfaces : `keywords = {prefpostface}` ;

* Articles : `keywords = {article}` ;

* Chapitres d'ouvrage collectif : `keywords = {chap}` ;

* Articles de dictionnaire et d'encyclopédie : `keywords = {artdictencyclo}` ;

* Compte rendus : `keywords = {compterendu}` ;

* Traductions : `keywords = {trad}` ;

* Autres publications : `keywords = {autre}` ;

  * Annexes d'articles : `keywords = {autre, autreannexes}` ;

  * Mises en ligne : `keywords = {autre, autrenet}` ;

  * Communications : `keywords = {autre, autrecom}` ;

  * Carnets de recherche : `keywords = {autre, autrecarnet}` ;

  * Vidéo : `keywords = {autre, autrevideo}` ;

* Vulgarisation : `keywords = {vulgarisation}` ;

  * Articles de magazine : `keywords = {vulgarisation, vulgarisationmag}` ;

  * Livres : `keywords = {vulgarisation, vulgarisationbook}` ;

  * Émissions ou interviews radio : `keywords = {vulgarisation, vulgarisationradio}` ;

  * Émissions ou interviews télé : `keywords = {vulgarisation, vulgarisationtv}` ;

  * Article de presse écrite : `keywords = {vulgarisation, vulgarisationjournal}` ;

  * Entretien ou interview sur internet : `keywords = {vulgarisation, vulgarisationitw}` ;

  * Inclassable : `keywords = {vulgarisation, vulgarisationautre}` ;

## Monographies

```latex
@book{<nom auteur><année>-<numero d'ordre>,
  author = {<nom>, <prénom> and <nom co-auteur>, <prénom co-auteur>},
  title = {<titre de la monographie>},
  publisher = {<maison d'édition>},
  series = {<collection dans laquelle est publiée le livre>},
  address = {<lieu d'édition (pas de nom de pays)>},
  date = {<année d'édition (possibilité de faire : YYYY-MM ou YYYY-MM-JJ>},
  pagetotal = {<nombre total de page>},
  language = {<code langue. français = fr, anglais = en, espagnol = es, italien = it>},
  doi = {<doi au format 10... (il faut enlever le doi.org/)>}
  url = {<url>},
  HAL_id = {<hal-id>},
  HAL_version = {v<X>},
  keywords = {monographie}
}
```

exemple : 

```latex
@book{lerouxth2020-19,
  author = {Le Roux, Thomas and Jarrige, François},
  title = {La Contamination du monde. Une histoire des pollutions à l'âge industriel},
  publisher = {Point Seuil},
  date = {2020},
  pagetotal = {656},
  language = {fr},
  address = {Paris},
  HAL_id = {hal-03027858},
  HAL_version = {v1},
  keywords = {monographie}
}

```

## Direction

### Direction d'ouvrage

```latex
@proceedings{<nom auteur><année>-<numero d'ordre>,
  editor = {<nom>, <prénom> and <nom co-directeur>, <prénom co-directeur>},
  editortype = {dir.},
  publisher = {<maison d'édition>},
  address = {<lieu d'édition (pas de nom de pays)>},
  date = {<année d'édition (possibilité de faire : YYYY-MM ou YYYY-MM-JJ>},
  pagetotal = {<nombre total de page>},
  language = {<code langue. français = fr, anglais = en, espagnol = es, italien = it>},
  title = {<titre du livre>},
  series = {<collection dans laquelle est publiée le livre>},
  doi = {<doi au format 10... (il faut enlever le doi.org/)>}
  url = {<url - uniquement s'il n'y a pas de doi>},
  HAL_id = {<hal-id>},
  HAL_version = {v<X>},
  keywords = {dirouvrage}
}
```

par exemple :

```latex
@proceedings{delaurenti2020-37,
  editor = {Delaurenti, Béatrice and Le Roux, Thomas},
  editortype = {dir.},
  publisher = {Vendémiaire},
  date = {2020},
  pagetotal = {448},
  language = {fr},
  title = {De la contagion},
  address = {Paris},
  HAL_id = {hal-02961450},
  HAL_version = {v1},
  keywords = {dirouvrage}
}
```

### Direction de numéro de revue

```latex
@proceedings{<nom auteur><année>-<numero d'ordre>,
  editor = {<nom>, <prénom> and <nom co-directeur>, <prénom co-directeur>},
  editortype = {dir.},
  publisher = {<maison d'édition>},
  address = {<lieu d'édition (pas de nom de pays)>},
  date = {<année d'édition (possibilité de faire : YYYY-MM ou YYYY-MM-JJ>},
  volume = {<numero de la revue>},
  number = {<numéro du volume>},
  title = {<nom de la revue>},
  subtitle = {<titre de la revue>},
  language = {<code langue. français = fr, anglais = en, espagnol = es, italien = it>},
  doi = {<doi au format 10... (il faut enlever le doi.org/)>}
  url = {<url - uniquement s'il n'y a pas de doi>},
  HAL_id = {<hal-id>},
  HAL_version = {v<X>},
  keywords = {dirrevue}
}
```

par exemple :

```latex
@proceedings{fridenson2020-54,
  editor = {Fridenson, Patrick},
  editortype = {dir.},
  publisher = {Comité d'histoire de la Sécurité sociale},
  volume = {13},
  date = {2020},
  pages = {177},
  language = {fr},
  title = {Revue d'histoire de la protection sociale},
  subtitle = {Un siècle de réforme des retraites},
  address = {Paris},
  keywords = {dirrevue}
}
```
## Éditions et éditions critiques

### Édition dans un livre :

```latex
@book{<nom auteur><année>-<numero d'ordre>,
  editor = {<nom>, <prénom> and <nom co-auteur>, <prénom co-auteur},
  editortype = {éd.},
  title = {<titre du livre>},
  publisher = {<maison d'édition>},
  address = {<lieu d'édition (pas de nom de pays)>},
  date = {<année d'édition (possibilité de faire : YYYY-MM ou YYYY-MM-JJ>},
  pages = {<nombre de page total>},
  language = {<code langue. français = fr, anglais = en, espagnol = es, italien = it>},
  keywords = {editions}
}
```

exemple : 

```latex
@book{blanquie2019-58,
  editor = {Blanquie, Christophe},
  editortype = {éd.},
  title = {Roger de Bussy-Rabutin et Marie Madeleine de Scudéry, Correspondance},
  publisher = {Classiques Garnier},
  date = {2019},
  pages = {375},
  language = {fr},
  address = {Paris},
  keywords = {editions}
}
```

### Édition dans un article

```latex
@article{<nom auteur><année>-<numero d'ordre>,
  journal = {<nom de la revue>},
  title = {<titre de l'article>},
  volume = {<numero de la revue>},
  number = {<numéro du volume>},
  date = {<année d'édition (possibilité de faire : YYYY-MM ou YYYY-MM-JJ>},
  author = {<nom>, <prénom> and <nom co-auteur>, <prénom co-auteur>},
  authortype = {éd.},
  language = {<code langue. français = fr, anglais = en, espagnol = es, italien = it>},
  pages = {<pages>},
  doi = {<doi au format 10... (il faut enlever le doi.org/)>}
  url = {<url - uniquement s'il n'y a pas de doi>},
  HAL_id = {<hal-id>},
  HAL_version = {v<X>},
  keywords = {editions}
}
```

par exemple :

```latex
@article{piron2020-64,
  journal = {Oliviana},
  title = {Pierre Jean Olivi, Quaestio de divino velle et scire},
  volume = {6},
  date = {2020},
  author = {Piron, Sylvain},
  authortype = {éd.},
  language = {fr},
  url = {https://journals.openedition.org/oliviana/977},
  keywords = {editions}
}
```

## Préface et postface

### Préface ou postface d'un livre collectif

```latex
@incollection{<nom auteur><année>-<numero d'ordre>,
  author [de la pré/postface] = {<nom>, <prénom> and <nom co-auteur>, <prénom co-auteur>},
  booktitle = {<titre de la monographie>},
  publisher = {<maison d'édition>},
  address = {<lieu d'édition (pas de nom de pays)>},
  title = {<titre de la pré/postface>},
  editor = {<nom>, <prénom> and <nom co-directeur>, <prénom co-directeur>},
  editortype = {dir.},
  note = {postface [uniquement si le mot "pré/postface" n'est pas dans le titre]},
  pages = {<pages de la pré/postface>},
  date = {<année d'édition (possibilité de faire : YYYY-MM ou YYYY-MM-JJ>},
  language = {<code langue. français = fr, anglais = en, espagnol = es, italien = it>},
  doi = {<doi au format 10... (il faut enlever le doi.org/)>}
  url = {<url>},
  HAL_id = {<hal-id>},
  HAL_version = {v<X>},
  keywords = {prefpostface}
}
```

exemple :

```latex
@incollection{chartier2020-75,
  author = {Chartier, Roger},
  booktitle = {L'Editeur à l'œuvre : Reconsidérer l'autorialité. Actes du colloque 11-13 octobre 2018},
  publisher = {Universié de Bâle},
  address = {Bâle},
  title = {Six remarques en guise de conclusion},
  editor = {Brancher, Dominique and Berjola, Giovanni and Burg, Gaëlle},
  editortype = {dir.},
  note = {postface},
  pages = {180–184},
  date = {2020},
  language = {fr},
  keywords = {prefpostface}
}
```

### Préface ou postface d'une monographie

```latex
@inbook{<nom auteur><année>-<numero d'ordre>,
  author [de la pré/postface] = {<nom>, <prénom> and <nom co-auteur>, <prénom co-auteur>},
  booktitle = {Arts de braconner. Une histoire matérielle de la lecture chez Michel de Certeau},
  publisher = {<maison d'édition>},
  title = {<titre de la pré/postface>},
  address = {<lieu d'édition (pas de nom de pays)>},
  bookauthor = {<nom>, <prénom> and <nom co-auteur>, <prénom co-auteur>},
  pages = {<pages>},
  date = {<année d'édition (possibilité de faire : YYYY-MM ou YYYY-MM-JJ>},
  language = {<code langue. français = fr, anglais = en, espagnol = es, italien = it>},
  doi = {<doi au format 10... (il faut enlever le doi.org/)>}
  url = {<url>},
  HAL_id = {<hal-id>},
  HAL_version = {v<X>},
  keywords = {prefpostface}
}
```
exemple :

```latex
@inbook{chartier2020-74,
  author = {Chartier, Roger},
  booktitle = {Arts de braconner. Une histoire matérielle de la lecture chez Michel de Certeau},
  publisher = {Garnier},
  title = {Préface},
  address = {Paris},
  bookauthor = {Frejomil, Andrés G.},
  pages = {13–16},
  date = {2020},
  language = {fr},
  keywords = {prefpostface}
}
```

## Articles

```latex
@article{<nom auteur><année>-<numero d'ordre>,
  title = {Introduction. Raconter la Shoah : 40 ans d'écrits personnels dans Le Monde juif},
  author = {<nom>, <prénom> and <nom co-auteur>, <prénom co-auteur>},
  journal = {<nom de la revue>},
  volume = {<numero de la revue>},
  number = {<numéro du volume>},
  pages = {<pages>},
  date = {2020},
  language = {<code langue. français = fr, anglais = en, espagnol = es, italien = it>},
  doi = {<doi au format 10... (il faut enlever le doi.org/)>}
  url = {<url - uniquement s'il n'y a pas de doi>},
  HAL_id = {<hal-id>},
  HAL_version = {v<X>},
  keywords = {article}
}
```
exemple :

```latex
@article{lyoncaen2020-91,
  title = {Introduction. Raconter la Shoah : 40 ans d'écrits personnels dans Le Monde juif},
  author = {Lyon-Caen, Judith},
  journal = {Revue d'Histoire de la Shoah},
  volume = {211},
  number = {1},
  pages = {13-30},
  date = {2020},
  language = {fr},
  doi = {10.3917/rhsho.211.0013},
  HAL_id = {halshs-03137733},
  HAL_version = {v1},
  keywords = {article}
}
```

## Chapitres

```latex
@incollection{<nom auteur><année>-<numero d'ordre>,
  author = {<nom>, <prénom> and <nom co-auteur>, <prénom co-auteur>},
  booktitle = {<titre du livre>},
  address = {<lieu d'édition (pas de nom de pays)>},
  series = {<collection dans laquelle est publiée le livre>},
  publisher = {<maison d'édition>},
  title = {<titre du chapire>},
  editor = {<nom>, <prénom> and <nom co-directeur>, <prénom co-directeur>},
  editortype = {dir.},
  pages = {<pages du chapitre>},
  date = {<année d'édition (possibilité de faire : YYYY-MM ou YYYY-MM-JJ>},
  language = {<code langue. français = fr, anglais = en, espagnol = es, italien = it>},
  doi = {<doi au format 10... (il faut enlever le doi.org/)>}
  url = {<url - uniquement s'il n'y a pas de doi>},
  HAL_id = {<hal-id>},
  HAL_version = {v<X>},
  keywords = {chap}
}
```

exemple :

```latex
@incollection{cerutti2020-209,
  author = {Cerutti, Simona},
  booktitle = {Dans les règles du métier. Les acteurs des normes professionnelles au Moyen Age et à l'époque moderne},
  address = {Palerme},
  serires = {Economic History Frameworks, 5},
  publisher = {New Digital Press},
  title = {Introduction},
  editor = {Bernardi, Philippe and Maitte, Corine and Rivière, François},
  editortype = {dir.},
  pages = {175-180},
  date = {2020},
  language = {fr},
  keywords = {chap}
}
```

## Articles de dictionnaire ou d'encyclopédie

Idem que "Chapitres", avec comme mot clef "artdictencyclo".

## Compte rendus

Idem que "Articles", avec comme mot clef "compterendu"

## Traductions

Idem que "Monographies" ou Articles", avec comme mot clef "trad".

L'auteur est l'auteur du texte originel.

Pour le traducteur : ajouter une ligne : `translator = {<nom>, <prénom>},`