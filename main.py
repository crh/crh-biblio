#!/usr/bin/python
# -*- coding: UTF-8 -*-

"""
Author : Jean-Damien Généro
Affiliation (en) : French National Centre for Scientific Research, Center for Historical Studies
Affiliation (fr) : Centre national de la recherche scientifique (CNRS), Centre de recherches historiques (CRH, UMR 8558)
Date : 2021-07-08
Update : 2021-07-09
"""

import time


from python.making_bib_files import mainbib_file, typological_files, annual_files, decades_files
from python.tex_compil import making_tex_file
from python.bib_refs_xtraction import sorted_all_date, old_1997_biblio_types, old_1997_typologies, \
                               new_2021_biblio_types, new_2021_typologies, decades, xxi_years, xxi_decades
from python.clean_up import make_keys


start_time = time.time()

# WRITTING MAIN BIB FILE = biblio-retro-all.bib
# mainbib_file(old_1997_biblio_types, "./bib/biblio-retro-all.bib")

# WRITTING TYPOLOGICAL FILES : 1996 TYPOS OR 2021 TYPO
# typological_files(old_1997_typologies, old_1997_biblio_types, "./bib/1997-typologies/")
# typological_files(new_2021_typologies, new_2021_biblio_types, "./bib/2021-typologies/")

# WRITTING ANNUAL FILES
# annual_files(sorted_all_date, old_1997_typologies, "./bib/annual-bibliography/")

# WRITTING DECENNIALS FILES
# decades_files(decades, "./bib")

# make_keys("./bib/2010-2013-biblio.bib")
make_keys("./bib/2017-biblio.bib")
make_keys("./bib/2018-biblio.bib")
make_keys("./bib/2019-biblio.bib")
make_keys("./bib/2020-biblio.bib")
make_keys("./bib/2021-biblio.bib")
make_keys("./bib/2022-biblio.bib")

# making_tex_file("./tex-canvas/quinqua_tex_canvas.tex", decades, "<DECADE>", ".")
making_tex_file("./tex-canvas/xxi_tex_canvas.tex", xxi_years, "<YEAR>", ".")
# making_tex_file("./tex-canvas/quinqua_tex_canvas.tex", xxi_decades, "<XXI>", ".")
# making_tex_file("./tex-canvas/xxi_tex_canvas.tex", xxi_decades, "<YEAR>", ".")

print("--- %s seconds ---" % (time.time() - start_time))
