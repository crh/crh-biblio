#!/usr/bin/python
# -*- coding: UTF-8 -*-

"""
Author : Jean-Damien Généro
Affiliation (en) : French National Centre for Scientific Research, Center for Historical Studies
Affiliation (fr) : Centre national de la recherche scientifique (CNRS), Centre de recherches historiques (CRH, UMR 8558)
Date : 2021-06-29
Update : 2021-07-09
"""

import os
import re


def making_tex_file(tex_canva, decades_dict, pattern, directory):
    """
    Opening a tex file, replacing a pattern in it by a decade (YYYY-YYYY), writting the result in a new tex file
    named after the decade, compilating it.
    :param tex_canva: path to a tex file with a variable <pattern> that will be replaced by the decade (YYYY-YYYY)
    :type tex_canva: str
    :param decades_dict: dict where <decade>: <first year - 1>
    :type decades_dict: dict
    :param pattern: variable in a tex file
    :type pattern: str
    :param directory: path to the directory containing tex file (<".">)
    :type directory: str
    """
    for decade in decades_dict:
        with open(tex_canva, 'r', encoding='utf-8') as opening:
            file = opening.read()
            if pattern == '<DECADE>':
                file = file.replace(pattern.replace('>', '-TEXT>'), decade.replace('-', ' à '))
                file = file.replace(pattern, decade)
            elif pattern == '<XXI>':
                file = file = file.replace('<DECADE>'.replace('>', '-TEXT>'), decade.replace('-', ' à '))
                file = file.replace('<DECADE>', decade)
                file = file.replace('quinqua-about.tex', 'xxi_about.tex')
                file = file.replace(r'\textbf{Bibliographie rétrospective}', r'\textbf{Bibliographie quinquennale}')
            elif pattern == '<YEAR>':
                file = file.replace(pattern, decade)
                if decade == '2020' or decade == '2018':
                    file = re.sub(r'(\\printbibliography\[heading=subbibliography,keyword=trad,heading=none\])', '% \\1', file)
                    file = re.sub(r'(\\printbibliography\[heading=subbibliography,keyword=vulgarisationtv,heading=none\])', r'% \\1\n\\textit{Néant.}', file)
                elif decade == '2021':
                    file = file.replace(r'\textit{Néant.}', r"% \textit{Néant.}")
                    file = re.sub(r'(\\printbibliography\[heading=subbibliography,keyword=trad,heading=none\])', '\\1', file)
                    file = re.sub(r'(\\printbibliography\[heading=subbibliography,keyword=vulgarisationautre,heading=none\])', r'% \\1\n\\textit{Néant.}', file)
                    file = re.sub(r'(\\printbibliography\[heading=subbibliography,keyword=vulgarisationbook,heading=none\])', r'% \\1\n\\textit{Néant.}', file)
                elif decade == '2019':
                    file = file.replace(r'\textit{Néant.}', r"% \textit{Néant.}")
                    file = re.sub(r'(\\printbibliography\[heading=subbibliography,keyword=vulgarisationradio,heading=none\])', r'% \\1\n\\textit{Néant.}', file)
                    file = re.sub(r'(\\printbibliography\[heading=subbibliography,keyword=vulgarisationautre,heading=none\])', r'% \\1\n\\textit{Néant.}', file)
                elif decade == '2022':
                    file = re.sub(r'(\\printbibliography\[heading=subbibliography,keyword=vulgarisationbook,heading=none\])', r'% \\1\n\\textit{Néant.}', file)
                    file = re.sub(r'(\\printbibliography\[heading=subbibliography,keyword=vulgarisationmag,heading=none\])', r'% \\1\n\\textit{Néant.}', file)
                    file = re.sub(r'(\\printbibliography\[heading=subbibliography,keyword=vulgarisationtv,heading=none\])', r'% \\1\n\\textit{Néant.}', file)
                    file = re.sub(r'(\\printbibliography\[heading=subbibliography,keyword=vulgarisationnet,heading=none\])', r'% \\1\n\\textit{Néant.}', file)
                    file = re.sub(r'(\\printbibliography\[heading=subbibliography,keyword=vulgarisationautre,heading=none\])', r'% \\1\n\\textit{Néant.}', file)
                elif decade == '2017':
                    file = file.replace(r'\textit{Néant.}', r"% \textit{Néant.}")
                    file = re.sub(r'(\\printbibliography\[heading=subbibliography,keyword=vulgarisationradio,heading=none\])', r'% \\1\n\\textit{Néant.}', file)
                    file = re.sub(r'(\\printbibliography\[heading=subbibliography,keyword=vulgarisationtv,heading=none\])', r'% \\1\n\\textit{Néant.}', file)
                elif decade == '2012-2016':
                    file = file.replace(r'\textbf{Bibliographie annuelle}', r'\textbf{Bibliographie quinquennale}')
                    file = file.replace(r'\textbf{1\up{er} janvier -- 31 décembre ', r'\textbf{')
                    file = file.replace(r'\textit{Néant.}', r"% \textit{Néant.}")
                    file = re.sub(r'(\\printbibliography\[heading=subbibliography,keyword=vulgarisationbook,heading=none\])', r'% \\1\n\\textit{Néant.}', file)
                    file = re.sub(r'(\\printbibliography\[heading=subbibliography,keyword=vulgarisationjournal,heading=none\])', r'% \\1\n\\textit{Néant.}', file)
                    file = re.sub(r'(\\printbibliography\[heading=subbibliography,keyword=vulgarisationmag,heading=none\])', r'% \\1\n\\textit{Néant.}', file)
                    file = re.sub(r'(\\printbibliography\[heading=subbibliography,keyword=vulgarisationradio,heading=none\])', r'% \\1\n\\textit{Néant.}', file)
                    file = re.sub(r'(\\printbibliography\[heading=subbibliography,keyword=vulgarisationtv,heading=none\])', r'% \\1\n\\textit{Néant.}', file)
                    file = re.sub(r'(\\printbibliography\[heading=subbibliography,keyword=vulgarisationautre,heading=none\])', r'% \\1\n\\textit{Néant.}', file)
            result = file
        current_file = os.path.join(directory, decade + "-biblio" + ".tex")
        with open(current_file, 'w', encoding="utf-8") as file:
            file.write("{}\n".format(result))
    texfiles = []
    unnecessaries_extensions = ["toc", "gz", "xml", "out", "blg", "bcf", "bbl", "aux"]
    for file in os.listdir(directory):
        if file.endswith(".tex"):
            print("***** COMPILATION N°1 *****")
            os.system("pdflatex ./{}".format(file))
            print("***** RUNNING BIBER *****")
            os.system("biber ./{}".format(file.replace('.tex', '')))
            print("***** COMPILATION N°2 *****")
            os.system("pdflatex ./{}".format(file))
            os.system("mv ./{}.pdf ./pdf-tex/".format(file.replace('.tex', '')))
            os.system("mv ./{}.log ./pdf-tex/".format(file.replace('.tex', '')))
            os.system("mv ./{} ./pdf-tex/".format(file))
            texfiles.append(str(file))
    for filename in texfiles:
        print("{} ----> Sucess !".format(filename))
    for file_extension in unnecessaries_extensions:
        os.system("rm *.{}".format(file_extension))
        print("*.{} ----> deleted !".format(file_extension))
