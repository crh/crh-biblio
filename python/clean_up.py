#!/usr/bin/python
# -*- coding: UTF-8 -*-

"""
Author : Jean-Damien Généro
Date : 2021-04-21
Update : 2022-04-05
"""


import re


def change_keyword() -> None:
    """
    Removing year from keywords.
    """
    path = input("Enter path : ")
    with open(path, 'r', encoding='utf-8') as reading:
        file = reading.read()
        file = re.sub(r'(monographie)\d{4}', r'\1', file)
        file = re.sub(r'(dirouvrage)\d{4}', r'\1', file)
        file = re.sub(r'(dirrevue)\d{4}', r'\1', file)
        file = re.sub(r'(editions)\d{4}', r'\1', file)
        file = re.sub(r'(prefpostface)\d{4}', r'\1', file)
        file = re.sub(r'(article)\d{4}', r'\1', file)
        file = re.sub(r'(chap)\d{4}', r'\1', file)
        file = re.sub(r'(artdictencyclo)\d{4}', r'\1', file)
        file = re.sub(r'(compterendu)\d{4}', r'\1', file)
        file = re.sub(r'(trad)\d{4}', r'\1', file)
        file = re.sub(r'(autre)\d{4}(, autre\w+)\d{4}', r'\1\2', file)
        file = re.sub(r'(vulgarisation)\d{4}(, vulgarisation\w+)\d{4}', r'\1\2', file)
    with open(path, 'w', encoding='utf-8') as writting:
        writting.write(file)
        print("{} ----> done !".format(path))


def make_keys(path) -> None:
    """
    Replacing  keys' numbers by new numbers from 1.
    :param path: path to a BibLaTeX file
    :type path: str
    """
    with open(path, 'r', encoding='utf-8') as reading:
        print("**** BEGINNING ANALYSING {} ****".format(path))
        file = reading.read()
        key_name_year = r"@\w+{[a-z]+[0-9]{4}"  # @entry{author2020
        # 0/ replacing ’ by ', & by \&
        file = file.replace("’", "'")
        file = file.replace(" &", " \&")
        # 1/ removing old keys numbers
        file = re.sub(r"(" + key_name_year + r")-\d+,", r"\1,", file)
        # 2/ detecting keys
        key_plus_lines = re.findall(r"((" + key_name_year + r")(.+\n.+\n.+))", file)
        print("**** {} KEYS DETECTED ****".format(len(key_plus_lines)))
        # 3/ renumbering
        counter = 0
        for item in key_plus_lines:
            counter += 1
            print("Key n°{} : {}".format(counter, item[1]))
            new_key = item[0].replace(item[1], item[1] + "-" + str(counter))
            file = file.replace(item[0], new_key)
    with open(path, 'w', encoding='utf-8') as writting:
        writting.write(file)
        print("**** DONE ****")


def titles_auth(path):
    """
    replaces caps authors by min authors
    :param path: path to a BibLaTeX file
    :type path: str
    """
    with open(path, 'r', encoding='utf-8') as opening:
        file = opening.read()
        authors = re.findall(r' +author += +{.+},', file)
        for author in authors:
            min_caps = author.casefold().title()
            min_caps = min_caps.replace("And", "and")
            file = file.replace(author, min_caps)
        result = file
    with open(path, 'w', encoding='utf-8') as writting:
        writting.write(result)


def titles_eds(path):
    """
    replaces caps editors by min editors
    :param path: path to a BibLaTeX file
    :type path: str
    """
    with open(path, 'r', encoding='utf-8') as opening:
        file = opening.read()
        editors = re.findall(r' +editor += +{.+},', file)
        for editor in editors:
            min_caps = editor.casefold().title()
            min_caps = min_caps.replace("And", "and")
            file = file.replace(editor, min_caps)
        result = file
    with open(path, 'w', encoding='utf-8') as writting:
        writting.write(result)


def clean_hal_dates(path) -> None:
    """
    clean references extracted from HAL
    :param path: path to a BibLaTeX file
    :type path: str
    """
    keys = ["TITLE", "AUTHOR", "URL", "BOOKTITLE", "EDITOR", "PUBLISHER",
            "PAGES", "YEAR", "HAL_ID", "HAL_VERSION", "JOURNAL", "ORGANIZATION",
            "VOLUME", "PAGETOTAL", "MONTH", "DOI", "KEYWORDS", "PDF", "BOOKtitle",
            "NOTE"]
    months = {"Jan": "01", "Feb": "02", "Mar": "03", "Apr": "04", "May": "05",
              "Jun": "06", "Jul": "07", "Aug": "08", "Sep": "09", "Oct": "11", "Dec": "12"}
    patern = r'year = {(\d+)},\n +month = (\w+),\n'
    with open(path, 'r', encoding='utf-8') as opening:
        file = opening.read()
        for key in keys:
            if key in file:
                file = file.replace(key, key.lower())
        file = file.replace("{\\'e}", "é")
        file = file.replace("{\\`e}", "è")
        file = file.replace("{\\c c}", "ç")
        file = file.replace("{\\oe}", "oe")
        file = file.replace("{\\^o}", "ô")
        file = file.replace("{\\textdegree}", "°")
        file = file.replace("{\\`a}", "à")
        file = file.replace("{\\'E}", "É")
        file = re.sub(r" ?`` ?", '"', file)
        file = re.sub(r" ? '' ?", '"', file)
        file = re.sub(r":hal(shs)?", "2021", file)
        file = re.sub(r"keywords = {.+},", "keywords = {},", file)
        file = file.replace("{{", "{")
        file = file.replace("}}", "}")
        for month in months:
            file = re.sub(r'year = {(\d+)},\n +month = ' + month + ',',
                'date = {\\1-' + months[month] + '},', file)
        file = re.sub(r'year = {(\d+)}', 'date = {\\1}', file)
        result = file
    with open(path, 'w', encoding='utf-8') as writting:
        writting.write(result)


def split_date(path) -> None:
    """
    transform field date in fields year, month, day
    :param path: path to a BibLaTeX file
    :type path: str
    """
    months = {"01": "Jan", "02": "Feb", "03": "Mar", "04": "Apr", "05": "May",
              "06": "Jun", "07": "Jul", "08": "Aug", "09": "Sep", "11": "Oct", "12": "Dec"}
    with open(path, 'r', encoding='utf-8') as opening:
        file = opening.read()
        file = re.sub(r'date( = {\d{4}},)', 'year\\1', file)
        for month in months:
            file = re.sub(r'( +)?date = \{(\d{4})-' + month + r'\},', '\\1year = {\\2},\n\\1month = ' + months[month] + ',', file)
            file = re.sub(r'( +)?date = \{(\d{4})-' + month + r'-(\d+)\},', '\\1year = {\\2},\n\\1month = ' + months[month] + ',\n\\1day = {\\3},', file)
        result = file
    with open(path, 'w', encoding='utf-8') as writting:
        writting.write(result)


# clean_hal_dates("../bib/2017-biblio.bib")
# split_date("../bib/2018-biblio.bib")

