#!/usr/bin/python
# -*- coding: UTF-8 -*-

"""
Author : Jean-Damien Généro
Affiliation (en) : French National Centre for Scientific Research, Center for Historical Studies
Affiliation (fr) : Centre national de la recherche scientifique (CNRS), Centre de recherches historiques (CRH, UMR 8558)
Date : 2021-05-27
Update : 2021-07-08
"""


# LIBRARIES
import os
import subprocess


from python.bib_refs_xtraction import sorted_all_date, old_1997_biblio_types, old_1997_typologies, \
                               new_2021_biblio_types, new_2021_typologies, decades


# MAKING GLOBAL FILE
def mainbib_file(mainlist, mainbibfile) -> None:
    """
    Writting a file with all bib reference from a list of lists.
    :param mainlist: list with lists of bibref (= old_1997_biblio_types)
    :type mainlist: list
    :param mainbibfile: path the main bib file (= ./retro-biblio/biblio-retro-all.bib)
    :type mainbibfile: str
    """
    with open(mainbibfile, 'w', encoding='utf-8') as bibfile:
        for ls_biblio in mainlist:
            for item in ls_biblio:
                bibfile.write('{}\n'.format(item))
                print("{} ----> Done !".format(mainbibfile))


# MAKING TYPOLOGICAL FILES
def typological_files(typologies_list, mainlist, path) -> None:
    """
    Writting 19 bib files according to the CRH 1997 typologies in <./retro-biblio>.
    :param typologies_list: list with typologies label (<old_1997_typologies> or <new_2021_typologies>)
    :type typologies_list: list
    :param mainlist: list with lists of bibref (<old_1997_biblio_types> or <new_2021_biblio_types>)
    :type mainlist: list
    :param path: path to the folder where the files will be created (<./retro-biblio/> or <./pdf-retro-bib>)
    :type path: str
    """
    # making a dict by merging typologies_list and mainlist
    # note that mainlist is made out of lists hence both typologies_list and mainlist have the same lenght
    # dict_ls = '<full bib ref [from mainlist]>': '<CRH type [from typologies_list]>'
    dict_ls = {typologies_list[item]: mainlist[item] for item in range(len(typologies_list))}
    for ls_biblio in dict_ls:
        with open(os.path.join(path, str(ls_biblio) + "-quinqua.bib"), 'w', encoding='utf-8') as biblio_file:
            for item in dict_ls[ls_biblio]:
                biblio_file.write('{}\n'.format(item))
        print("{}.bib ----> Done !".format(str(ls_biblio)))


# MAKING ANNUAL FILES
def annual_files(yearslist, typologies_list, path) -> None:
    """
    Creating annual files with bibref in <./annual-biblio>
    :param yearslist: list of years (<sorted_all_date>)
    :type yearslist: list
    :param typologies_list: list with typologies label (<old_1997_typologies>)
    :type typologies_list: list
    :param path: path to the folder where the files will be created (<./annual-bibliography/>)
    :type path: str
    """
    for year in yearslist:
        annual_list = []
        for doc_type in typologies_list:
            for doc in doc_type:
                if "year = {}".format(year) in doc:
                    annual_list.append(doc)
        current_file = os.path.join(path, year + "-bibliography" + ".bib")
        with open(current_file, 'w', encoding="utf-8") as file:
            for document in annual_list:
                file.write("{}\n\n".format(document))
        print("{} ---> Done !".format(os.path.join(path, year + "-bibliography" + ".bib")))
    subprocess.call(["rm", "./annual-bibliography/Date 17-bibliography.bib"])
    print("./annual-bibliography/Date 17-bibliography.bib ---> Deleted")


# MAKING DECENNIALS FILES
def decades_files(decades_dict, path) -> None:
    """
    Creating decennials files with bibrefs in <./decennial-bibliography>.
    Decades are 1947-1956, 1957-1966, 1967-1976, 1977-1986, 1987-1997.
    :param decades_dict: dict where <decade>: <first year - 1>
    :type decades_dict: dict
    :param path: path to the folder where the files will be created (<./decennial-bibliography>)
    :type path: str
    """
    for decade in decades_dict:
        decade_list = []
        for reflist in new_2021_biblio_types:
            for ref in reflist:
                counter = decades_dict[decade]
                if counter == 1986:
                    limit = counter + 10
                elif counter == 1949:
                    limit = counter + 16
                else:
                    limit = counter + 9
                while counter <= limit:
                    counter += 1
                    if "year = {}".format(counter) in ref:
                        decade_list.append(ref)
        current_file = os.path.join(path, decade + "-biblio" + ".bib")
        with open(current_file, 'w', encoding="utf-8") as file:
            for document in decade_list:
                file.write("{}\n".format(document))
        print("{} ---> Done !".format(current_file))
