#!/usr/bin/python
# -*- coding: UTF-8 -*-

"""
Authors : Jean-Damien Généro
Affiliation : French National Center for Scientific Research (CNRS)
Assigned at the Centre de recherches historiques (CRH, UMR 8558)
Date : 2022-07-26
Update : 2022-08-01

"""


import os
import re
import bibtexparser
from bibtexparser.bparser import BibTexParser
from tqdm import tqdm


def _parse_bib_db(bibfile):
    """parses a bib file
    :param bibfile: path to a bib file
    :type bibfile: str
    :return: bibtexparser object
    :rtype: <class 'bibtexparser.bibdatabase.BibDatabase'>"""
    parser = BibTexParser(common_strings=False)
    parser.ignore_nonstandard_types = False
    parser.homogenise_fields = True
    with open(bibfile, encoding='utf-8') as opening_file:
        data = bibtexparser.load(opening_file, parser)
    return data


def _make_authors_list(bibfile):
    """makes a list with authors' names.
    we need to go thought several lists
    since ref may have multiple authors
    final list is authors.
    :param bibfile: bibtexparser object
    :type bibfile: <class 'bibtexparser.bibdatabase.BibDatabase'>
    :return: list of authors names ['surname, forename', ...]
    :rtype: list"""
    individuals = []
    authors = []
    for item in tqdm(bibfile.entries, desc='Building authors list...'):
        try:
            item['author']
        except KeyError:
            if item['editor']:
                individuals.append(item['editor'])
            continue
        if item['author']:
            individuals.append(item['author'])
    for list_of_indiv in tqdm([item.split(' and ') for item in set(individuals)],
                              desc='Streamlining authors list...'):
        for indiv in list_of_indiv:
            authors.append(indiv)
    print(authors)
    return authors


def _add_authors_keywords(bib_db, authors_list):
    """
    (1) adds new keywords bases on authors'names
    (2) implementes them in the bib database
    :param bib_db: bibtexparser object
    :type bib_db: <class 'bibtexparser.bibdatabase.BibDatabase'>
    :param authors_list: list of names (['surname, forename', ...])
    :type authors_list: list
    :return: bibtexparser object
    :rtype: <class 'bibtexparser.bibdatabase.BibDatabase'>
    """
    data_base = bib_db
    # (1)
    for author in tqdm(sorted(set(authors_list)),
                       desc='Implementing new keywords...'):
        # print(author)
        # keywords are made out of the authors_list
        keyw_name = re.split(", ", author)
        unwanted = [" ", "-", "'"]
        letters = {"é": "e", "è": "e", "ë": "e", "ê": "e",
                    "ó": "o", "ò": "o", "ö": "o", "ú": "u",
                    "ü": "u", "î": "i", "í": "i", "ï": "i",
                    "â": "a", "à": "a", "ä": "a", "á": "a",
                    "É": "E", "È": "E", "ñ": "n", "ç": "c"}
        for pattern in unwanted:
            keyw_name[0] = keyw_name[0].replace(pattern, '')
            keyw_name[1] = keyw_name[1].replace(pattern, '')
        for letter in letters:
            keyw_name[0] = keyw_name[0].replace(letter, letters[letter])
            keyw_name[1] = keyw_name[1].replace(letter, letters[letter])
        keyw = keyw_name[0].lower() + '-' + keyw_name[1].lower()
        # (2)
        for item in data_base.entries:
            try:
                item['keywords']
            except KeyError:
                continue
            try:
                item['author']
            except KeyError:
                if author in item['editor']:
                    item['keywords'] = item['keywords'] + ', ' + keyw
                continue
            if author in item['author']:
                item['keywords'] = item['keywords'] + ', ' + keyw
    return data_base


def init(input_file, output_file)-> None:
    """writes bibfile with authors based keywords
    :param input_file: input bibtex file name
    :type input_file: str
    :param output_file: output bibtex file name
    :type output_file: str
    """
    path_to_bib = os.path.join('..', 'bib',)
    file = _parse_bib_db(os.path.join(path_to_bib, input_file))
    all_authors = _make_authors_list(file)
    final_base = _add_authors_keywords(file, all_authors)
    with open(os.path.join(path_to_bib, 'X2HAL', output_file), 'w') as bibtex_file:
        bibtexparser.dump(final_base, bibtex_file)


YEARS = ["2017"]

for year in YEARS:
    print("*** STARTING PROCESS FOR {}".format(year))
    entry_file = year + "-biblio.bib"
    exit_file = "authors-" + year + ".bib"
    init(entry_file, exit_file)
    print("*** SUCESS FOR {}\n".format(year))
